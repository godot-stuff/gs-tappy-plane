## gs-tappy-plane
The purpose of this Project is to demonstrate how the [gs-project-manager](https://gitlab.com/godot-stuff/gs-project-manager) can be used to organize a number of existing Sources into a new Project. In this case we are using the demo game [Tappy Plane](https://godotengine.org/asset-library/asset/272), which is available in the [Asset Library](https://godotengine.org/asset-library/asset).

### Installation
Make sure you have gspm installed and loaded, then enter the following on the command line in some folder on your computer.

```bash
> git clone https://gitlab.com/godot-stuff/gs-tappy-plane.git
> cd gs-tappy-plane
> gspm install
> gspm run
```

This will pull down the latest project file, install the needed assets, and then run the project.

### Project Structure
This project is a little different in that we are not storing any Source code along with the [project.yml](https://gitlab.com/godot-stuff/gs-project-manager/wikis/project) file. Instead all of the Source is located in the Godot Asset Library.

This would be useful in a Scenario where you might want to pull all of your source for running a Build script specific for one type of OS.

### The Assets
If you look at the project.yml, you will see three asset entries. The first is for the Game, Tappy Plane, next for a Asset library called Json Beautifier, and last for PersistenceNode.

The tool will install each asset in the order they are listed.

Pro Tip: Use the -v option with gspm to get a verbose listing of messages to see what is happening in the background.

### Repositories
The Project is pulling the Assets using their GIT repository locations. There is currently no facility for pulling anything from the Asset Library yet, although this is coming.

### Overwriting Assets
Notice that we completely ignore the addons that are included in the original Source, and instead replace them with the latest version from the Godot Asset Library.

This ensures that we are using the latest Code from the Asset creator.

The default is to always use the Branch specified on the Asset. If none is specified, it will always use Master. See the [Documentation](https://gitlab.com/godot-stuff/gs-project-manager/wikis/) for more information.

